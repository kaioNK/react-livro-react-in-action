import React, {Component} from 'react'
import PropTypes from 'prop-types'

class CreateComments extends Component{

  state = {
    content: '',
    user: ''
  }

  handleUserChange = (event) => {
    const val = event.target.value
    this.setState({user: val})
  }

  handleTextChange = (event) => {
    const val = event.target.value
    this.setState({content: val})
  }

  handleSubmit = (event) => {
    event.preventDefault()
    this.setState({user: '', content: ''})
  }

  render(){
    return(
      <form onSubmit={() => this.handleSubmit()}>
        <label>Name: </label>
        <input type='text' placeholder='Your name' value={this.state.user} onChange={() => this.handleUserChange()} />
        <br/>

        <label>Thoughts: </label>
        <input type='text' placeholder='Thoughts?' value={this.state.content} onChange={() => this.handleTextChange()}/>
        <br/>

        <input type='submit' />
      </form>
    )
  }
}

CreateComments.propTypes = {
  content: PropTypes.string.isRequired
}

export default CreateComments